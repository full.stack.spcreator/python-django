import {Fragment, useState} from "react";
import {Button, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";


const API_STATIC_MEDIA = 'http://127.0.0.1:8000'

const ModalImage = (props) => {
    const [visible, setVisible] = useState(false)
    const toggle = () => {
        setVisible(!visible)
    }
    return (
        <>
            <img onClick={toggle} src={API_STATIC_MEDIA + props.article.ar_img} alt='loading' style={{height: 50}}/>
            <Modal isOpen={visible} toggle={toggle}>
                <ModalHeader style={{color:"white",justifyContent: "center", backgroundColor:"#212529"}}>Image</ModalHeader>
                <ModalBody style={{display:"flex", justifyContent:"center", backgroundColor:"#212529"}}><img src={API_STATIC_MEDIA + props.article.ar_img} alt="loading" style={{width: 450}}/></ModalBody>
                <ModalFooter style={{display:"flex", justifyContent:"center", backgroundColor:"#212529"}}> <Button type="button" onClick={() => toggle()}>CLose</Button></ModalFooter>
            </Modal>
        </>
    )
}

export default ModalImage;