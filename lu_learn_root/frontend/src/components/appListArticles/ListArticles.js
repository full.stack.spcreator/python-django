import {Table} from "reactstrap";
import ModalImage from "../appImageModal/ImageModal";

const ListArticles = (props) => {
    const {articles} = props
    return (
        <Table dark>
            <thead>
            <tr>
                <th>Title</th>
                <th>Description</th>
                <th>Article text</th>
                <th>Data</th>
                <th>Author</th>
                <th>Image</th>
            </tr>
            </thead>
            <tbody>
            {!articles || articles.length <= 0 ? (
                <tr>
                    <td colSpan="6" align="center">
                        <b>Articles not found....</b>
                    </td>
                </tr>
            ) : articles.map(article => (
                    <tr key={article.id}>
                        <td>{article.ar_title}</td>
                        <td>{article.ar_short_description}</td>
                        <td>{article.ar_full_description}</td>
                        <td>{article.ar_dt}</td>
                        <td>{article.ar_author}</td>
                        <td><ModalImage
                            article={article}
                        /></td>
                    </tr>
                )
            )}
            </tbody>
        </Table>
    )
}

export default ListArticles