import {useEffect, useState} from "react";
import {Button, Form, FormGroup, Input, Label} from "reactstrap";
import axios from "axios";
import {API_URL} from "../../index";

const ArticleForm = (props) => {
    const [article, setArticle] = useState({})

    const onChange = (e) => {
        const newState = article
        if (e.target.name === "file") {
            newState[e.target.name] = e.target.files[0]
        } else newState[e.target.name] = e.target.value
        setArticle(newState)
    }

    useEffect(() => {
        if (!props.newArticle) {
            setArticle(article => props.article)
        }
        // eslint-disable-next-line
    }, [props.article])

    const defaultIfEmpty = value => {
        return value === "" ? "" : value;
    }

    const submitDataEdit = async (e) => {
        e.preventDefault();
        // eslint-disable-next-line
        const result = await axios.put(API_URL + article.id, article, {headers: {'Content-Type': 'multipart/form-data'}})
            .then(() => {
                props.resetState()
                props.toggle()
            })
    }
    const submitDataAdd = async (e) => {
        e.preventDefault();
        const data = {
            title: article['ar_title'],
            description: article['ar_short_description'],
            Data: article['ar_dt'],
            image: "/",
        }
        // eslint-disable-next-line
        const result = await axios.post(API_URL, data, {headers: {'Content-Type': 'multipart/form-data'}})
            .then(() => {
                props.resetState()
                props.toggle()
            })
    }
    return (
        <Form onSubmit={props.newArticle ? submitDataAdd : submitDataEdit}>
            <FormGroup>
                <Label for="title">Title:</Label>
                <Input
                    type="text"
                    name="title"
                    onChange={onChange}
                    defaultValue={defaultIfEmpty(article.ar_title)}
                />
            </FormGroup>
            <FormGroup>
                <Label for="description">Description:</Label>
                <Input
                    type="text"
                    name="description"
                    onChange={onChange}
                    defaultValue={defaultIfEmpty(article.ar_short_description)}
                />
            </FormGroup>
            <FormGroup>
                <Label for="document">Data:</Label>
                <Input
                    type="text"
                    name="data"
                    onChange={onChange}
                    defaultValue={defaultIfEmpty(article.ar_dt)}
                />
            </FormGroup>
            <FormGroup>
                <Label for="image">Image:</Label>
                <Input
                    type="file"
                    name="image"
                    onChange={onChange}
                    accept='image/*'
                />
            </FormGroup>
            <div style={{display: "flex", justifyContent: "space-between"}}>
                <Button>Send</Button> <Button onClick={props.toggle}>Cancel</Button>
            </div>
        </Form>
    )
}

export default ArticleForm;