import {Container, Row, Col} from "reactstrap";
import ListArticles from "../appListArticles/ListArticles";
import axios from "axios";
import {useEffect, useState} from "react";
import {API_URL} from "../../index";

const Home = () => {
    const [articles, setArticles] = useState([])

    useEffect(()=>{
        getArticles()
    },[])

    const getArticles = (data)=>{
        axios.get(API_URL).then(data => setArticles(data.data))
    }

    const resetState = () => {
        getArticles();
    };

    return (
        <Container style={{marginTop: "20px"}}>
            <Row>
                <Col>
                    <ListArticles articles={articles} resetState={resetState} newArticle={false}/>
                </Col>
            </Row>
        </Container>
    )
}

export default Home;