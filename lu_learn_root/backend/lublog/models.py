from django.db import models


class Article(models.Model):
    ar_dt = models.DateTimeField(auto_now=True, verbose_name='Data')
    ar_title = models.CharField(max_length=200, verbose_name='Title')
    ar_img = models.ImageField(upload_to='articleimg/', verbose_name='IMAGE', default='')
    ar_short_description = models.CharField(max_length=200, verbose_name='Short description')
    ar_full_description = models.TextField(verbose_name='Full description')
    ar_author = models.CharField(max_length=200, verbose_name='Author')

    def __str__(self):
        return self.ar_title

    class Meta:
        verbose_name = 'Article'
        verbose_name_plural = 'Articles'
