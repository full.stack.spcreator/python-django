from rest_framework import serializers
from .models import Article


class ArticleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Article
        fields = '__all__'
        # fields = ('ar_title', 'ar_short_description', 'ar_full_description', 'ar_author')
