from django.contrib import admin
from .models import Article
from django.utils.safestring import mark_safe

class ArticleAdmin(admin.ModelAdmin):
    list_display = ('ar_title', 'ar_short_description', 'ar_author', 'ar_dt', 'get_img')
    search_fields = ('ar_title', 'ar_author', 'ar_dt')
    list_editable = ('ar_short_description',)
    list_per_page = 10
    list_max_show_all = 50
    fields = ('ar_dt', 'ar_title', 'ar_short_description', 'ar_full_description', 'ar_img', 'ar_author', 'get_img')
    readonly_fields = ('ar_dt', 'get_img',)

    def get_img(self, obj):
        if obj.ar_img:
            print('obj.ar_img', obj.ar_img)
            return mark_safe(f'<a href="{obj.ar_img.url}"><img src="{obj.ar_img.url}" width="80px"></a>')
        else:
            return mark_safe('Empty')

    get_img.short_description = 'Preview'


admin.site.register(Article, ArticleAdmin)
