from rest_framework.views import APIView
from django.shortcuts import render
from .models import Article
from rest_framework.response import Response
from rest_framework import status
from .serializers import ArticleSerializer
from rest_framework.schemas.openapi import AutoSchema


class CustomSchema(AutoSchema):
    def get_operation(self, path, method):
        op = super().get_operation(path, method)
        op['parameters'].append({
            "name": "id",
            "in": "query",
            "required": False,
            "description": "Article ID",
            'schema': {'type': 'integer'}
        })
        op['parameters'].append({
            "name": "offset",
            "in": "query",
            "required": False,
            "description": "Offset",
            'schema': {'type': 'integer'}
        })
        op['parameters'].append({
            "name": "limit",
            "in": "query",
            "required": False,
            "description": "Limit",
            'schema': {'type': 'integer'}
        })
        return op


def get_articles(request, offset, limit):
    print('----- articles -----')

    if request.GET.get('offset'):
        offset = int(request.GET.get('offset'))
    if request.GET.get('limit'):
        get_limit = int(request.GET.get('limit'))
    if offset and get_limit:
        limit = offset + get_limit
        article_list = Article.objects.all()[offset:limit]
    elif limit:
        article_list = Article.objects.all()[:limit]
    else:
        article_list = Article.objects.all()

    dict_obj = {'article_list': article_list}
    return render(request, 'lublog/index.html', dict_obj)


def get_all_articles(request):
    article_list = Article.objects.all()
    dict_obj = {'article_list': article_list}
    return render(request, 'lublog/index.html', dict_obj)


def get_article(request):
    print('----- get_article -----')
    post_id = request.GET['id']
    article_list = Article.objects.all()
    post_data = Article.objects.get(id=post_id)
    return render(request, 'lublog/post.html', {'post_data': post_data, 'count': article_list})


class ArticleAPIView(APIView):
    serializer_class = ArticleSerializer
    schema = CustomSchema()

    def get(self, request):
        print('----- GET backend/article -----')
        post_id = request.query_params.get('id')
        offset = request.query_params.get('offset')
        limit = request.query_params.get('limit')

        if post_id:
            post = Article.objects.filter(id=post_id)
        elif offset and limit:
            limit = int(offset) + int(limit)
            post = Article.objects.all()[int(offset):limit]
        elif limit:
            post = Article.objects.all()[:limit]
        else:
            post = Article.objects.all()

        if post:
            post_serializer = self.serializer_class(post, many=True)
            return Response(post_serializer.data, status=status.HTTP_200_OK)
        else:
            return Response({'message': 'No posts found'}, status=status.HTTP_200_OK)