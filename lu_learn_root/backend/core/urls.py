from django.contrib import admin
from django.urls import path, include
from lublog import views
from django.conf.urls.static import static
from django.conf import settings
from rest_framework.schemas import get_schema_view
from django.views.generic import TemplateView

urlpatterns = [
    path('backend/', include('lublog.urls')),
    path('admin/', admin.site.urls),
    path('', views.get_articles, kwargs={'offset': 0, 'limit': 10}),
    path('articles', views.get_all_articles),
    path('article/', views.get_article, name='article'),
    path('api_schema/', get_schema_view(
        title='API Schema',
        description='Guide for the REST API'
    ), name='api_schema'),
    path('docs/', TemplateView.as_view(
        template_name='docs.html',
        extra_context={'schema_url': 'api_schema'}
    ), name='swagger-ui'),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
