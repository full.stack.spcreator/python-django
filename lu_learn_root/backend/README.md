<h1>Learn project for LearnUP</h1>

<h1>1. INSTALL PROJECT</h1>
Project has two ways to install and work:
<ul>
    <li>use local python version</li>
    <li>use virtual environment python version (venv)</li>
</ul>

<h2>1.1 Use local python version</h2>
Use the stack commands from point 1.3 in the "lulearnproject" folder. 

<h2>1.2 Use virtual environment python version (venv)</h2>
First you need to create in the "learn project" folder a python virtual environment by running the console command:
<ol>
    <li>sudo apt install python3 python3-pip python3-venv</li>
    <li>. venv/bin/activate</li>
</ol>

After that, we execute the commands from paragraph 1.3 (except for points 1 to 3) in the interactive console of the virtual environment.

<h2>1.3 Create DB</h2>
<ol>
    <li>Create DB 'lublog'</li>
    <li>Change USER & PASSWORD in settings.py in core folder (DATABASES = {})</li>
</ol>

<h2>1.4 Console command for backend install</h3>
<ol>
    <li>sudo apt install python3</li>
    <li>sudo apt install python3-pip</li>
    <li>sudo apt install python3-venv</li>
    <li>pip install -r requirements.txt</li>
    <li>python manage.py createsuperuser</li>
    <li>python manage.py makemigrations</li>
    <li>python manage.py migrate</li>
    <li>python manage.py createsuperuser</li>
</ol>

<h2>1.4 Console command for frontend install</h3>
Use console command in "reactapp" folder:
<ul>
    <li>npm install</li>

[//]: # (    <li>npm i axios reactstrap bootstrap</li>)
</ul>

<h1>2. START PROJECT</h1>
<b>backend</b>: python manage.py runserver <br>
<b>frontend</b>: npm start

<h1>3. ROUTING</h1>

<b>Admin page</b>: http://127.0.0.1:8000/admin <br>
<b>Swagger page</b>: http://127.0.0.1:8000/docs/ <br>
<b>Front without React.js</b>: http://127.0.0.1:8000/articles <br>
<b>Front with React.js</b>: http://localhost:3000/ <br>
