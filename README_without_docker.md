<h1>Learn project for LearnUP</h1>

<h1>1. INSTALL PROJECT</h1>

<h2>1.1 Console command for backend install</h2>

<ol>
    <li>git clone git@gitlab.com:full.stack.spcreator/python-django.git</li>
    <li>cd python-django</li>
    <li>sudo apt install python3 python3-pip python3-venv</li>
    <li>pip install -r requirements.txt</li>
    <li>python manage.py createsuperuser</li>
    <li>python manage.py makemigrations</li>
    <li>python manage.py migrate</li>
    <li>python manage.py createsuperuser</li>
</ol>

<h2>1.2 Create DB</h2>

<ol>
    <li>Create DB 'lublog'</li>
    <li>Change USER & PASSWORD in settings.py in "core" folder (DATABASES = {}) from your PostgreSQL</li>
</ol>

<h2>1.3 Console command for frontend install</h3>

Use console command in "reactapp" folder:

<ul>
    <li>npm install</li>
</ul>

<h1>2. START PROJECT</h1>

<b>backend</b>: python manage.py runserver <br>
<b>frontend</b>: npm start

<h1>3. ROUTING</h1>

<b>Admin page</b>: http://127.0.0.1:8000/admin <br>
<b>Swagger page</b>: http://127.0.0.1:8000/docs/ <br>
<b>Front without React.js</b>: http://127.0.0.1:8000/articles <br>
<b>Front with React.js</b>: http://localhost:3000/ <br>
