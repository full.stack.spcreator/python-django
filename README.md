<h1>Learn project for LearnUP</h1>

<h1>INSTALL PROJECT</h1>

<ol>
    <li>git clone git@gitlab.com:full.stack.spcreator/python-django.git</li>
    <li>docker-compose build</li>
    <li>docker-compose up -d</li>
    <li>docker-compose exec django-backend python manage.py migrate</li>
    <li>docker-compose exec django-backend python manage.py createsuperuser</li>
</ol>


<h1>ROUTING</h1>

<b>Admin page</b>: http://127.0.0.1:8000/admin <br>
<b>Swagger page</b>: http://127.0.0.1:8000/docs/ <br>
<b>Front without React.js</b>: http://127.0.0.1:8000/articles <br>
<b>Front with React.js</b>: http://localhost:3000/ <br>
